package main

import "fmt"

func main() {
	var number int
	fmt.Printf("Enter Number: ")
	fmt.Scanf("%d", &number)
	if validation(number) {
		text := getTextNumber(number)
		reverseText := reverseText(text)
		printOutput(number, text, reverseText)
	}
}

func validation(number int) bool {
	if number <= 0 {
		fmt.Println("warning: input value must greater 0")
		return false
	}
	return true
}

func printOutput(number int, text1, text2 string) {
	for i := 1; i <= number; i++ {
		if i%2 > 0 {
			fmt.Println(text1)
		} else {
			fmt.Println(text2)
		}
	}
}

//versi 1
//func for get number in range 1 to n in one line string
func getTextNumber(number int) string {
	var stringNumber string
	for i := 1; i <= number; i++ {
		stringNumber = stringNumber + fmt.Sprintf("%v", i)
	}
	return stringNumber
}

//func for reverse string
func reverseText(text string) string {
	var reverseString string
	lenText := len(text) - 1
	for i := lenText; i >= 0; i-- {
		reverseString = reverseString + string(text[i])
	}
	return reverseString
}
