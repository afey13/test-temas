package main

import "fmt"

func main() {
	var number int
	fmt.Printf("Enter Number: ")
	fmt.Scanf("%d", &number)
	if validation(number) {
		listNumber := getListNumber(number)
		text, reverseText := getTextNumber(listNumber, number)
		printOutput(number, text, reverseText)
	}
}

func validation(number int) bool {
	if number <= 0 {
		fmt.Println("warning: input value must greater 0")
		return false
	}
	return true
}

func printOutput(number int, text1, text2 string) {
	for i := 1; i <= number; i++ {
		if i%2 > 0 {
			fmt.Println(text1)
		} else {
			fmt.Println(text2)
		}
	}
}

//versi 2
func getListNumber(number int) []string {
	var listNumber []string
	for i := 1; i <= number; i++ {
		listNumber = append(listNumber, fmt.Sprintf("%v", i))
	}
	return listNumber
}

func getTextNumber(listNumber []string, number int) (string, string) {
	var textNumber, textWithReverseNumber string
	len := number - 1
	for i := 0; i <= len; i++ {
		textNumber = textNumber + listNumber[i]
		textWithReverseNumber = textWithReverseNumber + listNumber[len-i]
	}
	return textNumber, textWithReverseNumber
}
