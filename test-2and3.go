package main

import (
	"encoding/json"
	"fmt"
)

type Character struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	HitPoints    int    `json:"hitPoints"`
	Strength     int    `json:"strength"`
	Defense      int    `json:"defense"`
	Intelligence int    `json:"intelegence"`
	Class        int    `json:"class"`
}

type DTO struct {
	Data    []Character `json:"data"`
	Success bool        `json:"success"`
	Message *string     `json:"message"`
}

func main() {
	var data []Character
	var name string
	var intelegence, strength, defense, hitPoints, class int
	counter := 0
	fmt.Println(fmt.Sprintf("Character ke %d", counter+1))
	fmt.Printf("Name : ")
	fmt.Scanf("%s\n", &name)
	for name != "" {
		fmt.Printf("HitPoints : ")
		fmt.Scanf("%d\n", &hitPoints)
		fmt.Printf("Strength : ")
		fmt.Scanf("%d\n", &strength)
		fmt.Printf("Defense : ")
		fmt.Scanf("%d\n", &defense)
		fmt.Printf("Intelegence : ")
		fmt.Scanf("%d\n", &intelegence)
		fmt.Printf("Class : ")
		fmt.Scanf("%d\n", &class)
		character := Character{
			ID:           counter,
			Name:         name,
			HitPoints:    hitPoints,
			Strength:     strength,
			Defense:      defense,
			Intelligence: intelegence,
			Class:        class,
		}
		data = append(data, character)
		fmt.Println(name, hitPoints, class)
		counter++
		name = ""
		fmt.Println(fmt.Sprintf("Character ke %d", counter+1))
		fmt.Printf("Name : ")
		fmt.Scanf("%s\n", &name)
		fmt.Println(name)
	}
	dto := DTO{
		Data:    data,
		Success: true,
		Message: nil,
	}
	stringDTO, _ := json.Marshal(dto)
	fmt.Println(string(stringDTO))
}
