package main

import "fmt"

const canDivBy35 string = "FizzBuzz"
const canDivBy3 string = "Fizz"
const canDivBy5 string = "Buzz"

func main() {
	var numberFrom, numberTo int
	fmt.Printf("Enter Number From: ")
	fmt.Scanf("%d", &numberFrom)
	fmt.Printf("Enter Number To: ")
	fmt.Scanf("%d", &numberTo)
	if validation(numberFrom, numberTo) {
		printOutput(numberFrom, numberTo)
	}
}

func validation(numberFrom, numberTo int) bool {
	if numberFrom < 0 || numberTo < 0 {
		fmt.Println("warning: input value must greater than 0")
		return false
	}
	if numberFrom > numberTo {
		fmt.Println("warning: value Number From must lower number To")
		return false
	}
	return true
}

func printOutput(numberFrom, numberTo int) {
	counter := 0
	for i := numberFrom; i <= numberTo; i++ {
		div3 := i % 3
		div5 := i % 5
		if div5 == 0 && div3 == 0 {
			fmt.Println(canDivBy35, i)
		} else if div3 == 0 && div5 > 0 {
			fmt.Println(canDivBy3, i)
		} else if div5 == 0 && div3 > 0 {
			fmt.Println(canDivBy5, i)
		} else {
			counter += 1
		}
	}
	if counter > 0 {
		fmt.Println("there are no number can div by 3 or 5 or both")
	}
}
